
public class Int2Bin {

	public Int2Bin() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args){
		System.out.print(int2Bin(98));
	}
	
	public static String int2Bin (int number){
		if((number==0) || (number==1))
			return number + "";
		return int2Bin(number/2) + (number%2);
	}

}
